#+TITLE: A Open Gov movement
#+AUTHOR: Devi Prasad & Thirumal Ravula
#+DATE: [2017-09-17 Sun]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Introduction
  This document is an effort to see open government in a new
  light.  When it comes to government, we tend to silo the
  actual governance and the fight to get the mandate to
  governance - elections.  The challenge is to build a
  platform that provides the ability to feed the data of
  governance to the process of selecting the same
  governance.

* First Steps

* Hartal Platform

* Machine Learning
